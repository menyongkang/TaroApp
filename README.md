# TaroApp

#### 介绍

Taro demo

#### 软件架构

软件架构说明

#### 安装教程

1.  CLI 工具安装 npm install -g @tarojs/cli / yarn global add @tarojs/cli /cnpm install -g @tarojs/cli
2.  项目初始化 taro init myApp
3.  依赖初始化 yarn / cnpm install /npm install
4.  编译运行

    Taro 编译分为 dev 和 build 模式：

    dev 模式（增加 --watch 参数） 将会监听文件修改。
    build 模式（去掉 --watch 参数） 将不会监听文件修改，并会对代码进行压缩打包。
    dev 模式生成的文件较大，设置环境变量 NODE_ENV 为 production 可以开启压缩，方便预览，但编译速度会下降。

[Taro 官网](http://taro-docs.jd.com/taro/docs/GETTING-STARTED/)

#### 使用说明

[Taro 官网](http://taro-docs.jd.com/taro/docs/GETTING-STARTED/)
