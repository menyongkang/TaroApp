import { combineReducers } from 'redux'
import counter from './counter'

// 导出所有reducer
export default combineReducers({
  counter
})