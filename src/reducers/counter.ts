import { ADD, MINUS } from "../constants/counter"


// 定义初始化状态
const INITIAL_STATE = {
  num: 0
}

// 定义reducer
const counter = (state = INITIAL_STATE, action: { type: any }) => {
  // 整个State就只有一个
  const { type } = action
  switch (type) {
    case ADD:
      // 最终返回的新的State
      return {
        ...state,
        num: state.num + 1
      }
    case MINUS:
      return Object.assign({}, state, { num: state.num - 1 })
    default:
      return state

  }

}

export default counter