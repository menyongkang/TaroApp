// 开发
const dev = {
  domain: "https://test-open-gateway.enbrands.com"
};
// 生产
const prod = {
  domain: "https://jifenn-open.enbrands.com"
};

module.exports = ((env = process.env.NODE_ENV) => {
  let config;
  switch (env) {
    case "development":
      config = dev;
      break;
    case "production":
      config = prod;
      break;
    default:
      config = dev;
  }
  return config;
})();
