export interface ThreadProps {
  tid: string;
  not_navi: string;
  title: string;
  member: any;
  last_modified: number;
  replies: string;
  node: any;
}