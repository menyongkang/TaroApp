import Taro from "@tarojs/taro"
import React, { FC, useState } from 'react'
import { View, Input } from '@tarojs/components'
import { AtIcon, AtButton, AtToast } from "taro-ui";
import api from "../../services/http/api";
import './index.styl'

type InputType = "account" | "password";
const Login: FC = () => {
  const [showLoading, setShowLoading] = useState<boolean>(false);
  const [showTip, setShowTip] = useState<boolean>(false);
  const [account, setAccount] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [tip, setTip] = useState<string>("");

  function useLoginStatus(res) {
    const { retCode, data } = res.data;
    let tip = "登录成功";
    if (retCode !== 0) {
      tip = res.data.msg || "登录失败";
    }
    setShowLoading(false);
    setShowTip(true);
    setTip(tip);
    setTimeout(() => {
      setShowTip(false);
    }, 2000);
    if (retCode === 0) {
      Taro.setStorageSync("USERINFO", data);
      Taro.setStorageSync("TOKEN", data.token);
      Taro.setStorageSync("ACCOUNT", data.name);
      Taro.navigateTo({
        url: "/pages/sms/sms-list/index"
      });
    }
  }

  function login() {
    if (!account) {
      this.setState({
        showTip: true,
        tip: "请输入账号"
      });
      return;
    }
    if (!password) {
      this.setState({
        showTip: true,
        tip: "请输入密码"
      });
      return;
    }
    setShowLoading(true);
    api
      .post("/manage/user/login", {
        account,
        password: window.btoa(password)
      })
      .then(res => {
        useLoginStatus(res);
      });
  }

  function handleChange(type: InputType, event) {
    const { value } = event.detail;
    if (type === "account") {
      setAccount(value);
    } else {
      setPassword(value);
    }
  }
  return (
    <View className="login_container">
      <View className="login_content">
        <View className="login_content__item">
          <AtIcon value="iphone" size="24" color="#ccc"></AtIcon>
          <Input
            type="text"
            placeholder="账号"
            className="login_content__input"
            onInput={(e): void => {
              handleChange("account", e);
            }}
          />
        </View>
        <View className="login_content__item">
          <AtIcon value="lock" size="24" color="#ccc"></AtIcon>
          <Input
            type="text"
            password
            placeholder="密码"
            className="login_content__input"
            onInput={(e): void => {
              handleChange("password", e);
            }}
          />
        </View>
        <AtButton className="login_content__btn" onClick={() => login()}>
          登录
        </AtButton>
      </View>
      <AtToast
        isOpened={showLoading}
        text="登录中"
        status="loading"
        hasMask
        duration={30000000}
      ></AtToast>
      <AtToast isOpened={showTip} text={tip} hasMask duration={2000}></AtToast>
    </View>
  );
}


export default Login