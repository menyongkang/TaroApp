
import React, { Component } from 'react'
// 内置组件
import { View, Text } from '@tarojs/components'

class SmsRecharge extends Component {

  state = {
    msg: "Hollow World!",
  }

  render() {
    const { msg } = this.state
    return (
      <View className='index'>
        <Text>{msg}</Text>

      </View >
    )
  }
}

export default SmsRecharge