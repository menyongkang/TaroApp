import Taro from "@tarojs/taro"
import React, { Component } from 'react'
// 内置组件
import { View, Text } from '@tarojs/components'
import { AtButton, AtCheckbox, AtInput } from 'taro-ui'

import "taro-ui/dist/style/components/button.scss" // 按需引入
import './index.styl'


class SmsList extends Component {

  state = {
    msg: "Hollow World!",
    list: [
      {
        value: '美国',
        label: '美国',
      },
      {
        value: '中国',
        label: '中国',
        checked: true
      },
      {
        value: '巴西',
        label: '巴西',
      },
      {
        value: '日本',
        label: '日本',
      },
      {
        value: '英国',
        label: '英国',

      },
      {
        value: '法国',
        label: '法国',
      }
    ]
  }
  onReady() {
    console.log("onReady");
  }
  goToHome = () => {
    //路由跳转
    Taro.navigateTo({
      url: "/pages/home/index"
    });
  }
  render() {
    const { msg } = this.state
    return (
      <View className='index'>
        <Text>{msg}</Text>
        <AtCheckbox options={this.state.list} selectedList={[]} onChange={() => { }} />
        <AtInput type='text' placeholder='将会获取焦点' name='' onChange={() => { }} />
        <AtButton type='primary' onClick={() => {
          this.goToHome()
        }}>I need Taro UI</AtButton>
        <View>
        </View>
        <Text>flex-direction: row 横向布局</Text>
        <View className='flex-wrp' style='flex-direction:row;'>
          <View className='flex-item demo-text-1' >8888</View>
          <View className='flex-item demo-text-2' >555</View>
          <View className='flex-item demo-text-3' >666355</View>
        </View>
        <Text>flex-direction: column 纵向布局</Text>
        <View className='flex-wrp' style='flex-direction:column;'>
          <View className='flex-item flex-item-V demo-text-1' >y</View>
          <View className='flex-item flex-item-V demo-text-2' >y</View>
          <View className='flex-item flex-item-V demo-text-3' >y</View >
        </View >
      </View >
    )
  }
}

export default SmsList