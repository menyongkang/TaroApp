// import Taro from "@tarojs/taro"
import React, { FC } from 'react'
import { View } from '@tarojs/components'

import './index.styl'


const NotFound: FC = () => {
  return (
    <View >
      404
    </View >
  )
}

export default NotFound