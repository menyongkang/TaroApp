
// 全局配置文件/项目入口配置
export default {
  // 页面路径,数组的第一项带班表小程序的初始页
  pages: [
    'pages/login/index',
    "pages/not-found/index",
    "pages/no-access/index",
    "pages/sms/sms-list/index",
    "pages/sms/sms-details/index",
    "pages/sms/sms-recharge/index",
    "pages/sms/sms-recharge-record/index"
  ],
  window: {
    // 导航栏背景颜色
    navigationBarBackgroundColor: '#7c6dff',
    // 导航栏标题文字内容
    navigationBarTitleText: '短信平台',
    // 导航栏标题颜色
    navigationBarTextStyle: '#7c6dff',
    // 是否开启当前页面的下拉刷新。
    enablePullDownRefresh: true,
    // 下拉 loading 的样式
    backgroundTextStyle: 'light',
    // 窗口的背景色
    backgroundColor: "#000",
    // 屏幕旋转设置
    pageOrientation: "auto"
  }
}
