// 项目入口文件-- 入口组件
import React, { Component } from "react";
import { Provider } from "react-redux";
import configStore from "./store";

import "./app.styl";
// eslint-disable-next-line import/first
import 'taro-ui/dist/style/index.scss'
// 全局引入一次即可

const store = configStore();
class App extends Component {
  componentDidMount() { }
  // 程序启动，或切前台时触发
  componentDidShow() { }
  // 程序切后台时触发。
  componentDidHide() { }

  componentDidCatchError() { }

  // this.props.children 是将要会渲染的页面
  render() {
    return (
      <Provider store={store}>
        {this.props.children}
      </Provider>
    )
  }
}
// 每一个入口组件都必须导出一个 React 组件
export default App