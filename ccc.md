1. attrType =“string” && enumerateValue ：真

结构：

{
op: "contain",
attr: "",
values: []
}

---

2. attrType =“string” && enumerateValue： 假

结构：

{
attr: "",
op: "like",
value: ""
}

---

3. attrType =“number” && enumerateValue： 真

结构：
{
op: "contain",
attr: "",
values: []
}

---

4. attrType =“number” && enumerateValue ：假

结构：

op==='between'

{
attr: "",
op: "between",
min: undefined,
max: undefined
}

op!=='between'

{
attr: "",
op: "equal",
value: undefined
}

---

5. attrType =“boolean”

结构：
{
attr: "",
op: "equal",
value: undefined
}

---

6. attrType =“selection”

结构：
{
attr: "",
op: "bContain",
values: [],// 大数据用
goodIds: [],//前端回显
skuIds: []//前端回显
}

---

7. attrType =“selection_act”

结构：
{
attr: "",
op: "contain",
values: []
}

---

8. attrType =“selection_promotion”

结构：
{
attr: "",
op: "bHashOnlyContain",
values: []
}

---

9. attrType =“selection_gift”

结构：
{
attr: "",
op: "contain",
values: []
}

---

10. attrType =“selection_sku”

结构：
{
attr: "",
op: "contain",
values: []
}

---

11. attrType =“mult_selection”

结构：
{
attr:"",
op: "",
child_condition: []
};

type==="absolute_time" && op === "between"
{
attr:"",
op: "",
child_condition: [
{
type: "absolute_time",
op: "between",
min: undefined,
max: undefined
}

]
};

type==="absolute_time" && op !== "between"

{ {
attr:"",
op: "",
child_condition: [
{
type: "absolute_time",
op: "lte",
value: ""
}
]
};

type!=="absolute_time"
{
attr:"",
op: "",
child_condition: [
{
type: "relative_time",
op: "lte",
value: undefined
}

]
};

---

12. attrType =“selection_sku”

结构：
{
attr: "",
op: "equal",
value: 0
}
