const config = {
  // 项目名称
  projectName: "bi-admin-app",
  // 项目创建日期
  date: "2021-10-19",
  // 设计稿尺寸
  designWidth: 750,
  // 设计稿尺寸换算规则
  deviceRatio: {
    640: 2.34 / 2,
    750: 1,
    828: 1.81 / 2
  },
  // 项目源码目录
  sourceRoot: "src",
  // 项目产出目录
  outputRoot: "dist",
  // Taro 插件配置
  plugins: [
    // "@tarojs/plugin-stylus" // 使用 Stylus
  ],
  // 全局变量设置
  defineConstants: {},
  // 文件 copy 配置
  copy: {
    patterns: [],
    options: {}
  },
  // 框架，react，nerv，vue, vue3 等
  framework: "react",
  // 小程序端专用配置
  mini: {
    postcss: {
      pxtransform: {
        enable: true,
        config: {}
      },
      // 小程序端样式引用本地资源内联配置
      url: {
        enable: true,
        config: {
          limit: 1024 // 设定转换尺寸上限
        }
      },
      cssModules: {
        enable: false, // 默认为 false，如需使用 css modules 功能，则设为 true
        config: {
          namingPattern: "module", // 转换模式，取值为 global/module
          generateScopedName: "[name]__[local]___[hash:base64:5]"
        }
      }
    },
    // 自定义 Webpack 配置
    devServer: {
      // 自动打开浏览器
      open: true,
      // 启用 gzip compression
      compress: true,
      // 启用webpack 的热模块替换
      hot: true,
      // 制定监听请求端口号
      port: 3008,
      proxy: {
        "/manage": {
          target: "http://localhost:3008",
          // 默认不接受HTTPS 上运行且证书无效的后端服务器
          secure: false,
          // 默认情况下，代理时会保留主机头的来源，
          changeOrigin: true,
          pathRewrite: { "^/manage": "" }
        }
      }
    }
  },
  // H5 端专用配置
  h5: {
    publicPath: "/",
    staticDirectory: "static",
    postcss: {
      autoprefixer: {
        enable: true,
        config: {}
      },
      cssModules: {
        enable: false, // 默认为 false，如需使用 css modules 功能，则设为 true
        config: {
          namingPattern: "module", // 转换模式，取值为 global/module
          generateScopedName: "[name]__[local]___[hash:base64:5]"
        }
      }
    },
    // 自定义 Webpack 配置
    devServer: {
      // 自动打开浏览器
      open: true,
      // 启用 gzip compression
      compress: true,
      host: "localhost",
      // 启用webpack 的热模块替换
      hot: true,
      // 制定监听请求端口号
      port: 3008,
      proxy: {
        "/manage": {
          target: "http://localhost:3008",
          // 默认不接受HTTPS 上运行且证书无效的后端服务器
          secure: false,
          // 默认情况下，代理时会保留主机头的来源，
          changeOrigin: true,
          pathRewrite: { "^/manage": "" }
        }
      }
    }
  }
};

module.exports = function(merge) {
  if (process.env.NODE_ENV === "development") {
    return merge({}, config, require("./dev"));
  }
  return merge({}, config, require("./prod"));
};
