// 声明文件：给js 代码补充类型标注，这样在ts 编译环境下 就不会提示 js 文件“缺少类型”
declare module "*.png";
declare module "*.gif";
declare module "*.jpg";
declare module "*.jpeg";
declare module "*.svg";
declare module "*.css";
declare module "*.less";
declare module "*.scss";
declare module "*.sass";
declare module "*.styl";

declare interface Window {
  __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
  [key: string]: any;
}

// @ts-ignore
declare const process: {
  env: {
    TARO_ENV: 'weapp' | 'swan' | 'alipay' | 'h5' | 'rn' | 'tt' | 'quickapp' | 'qq' | 'jd';
    [key: string]: any;
  }
}